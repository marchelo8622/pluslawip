<?php

class ConexionMySQL{

	private $conexion; 
	private $total_consultas;
	private $nombre_fichero = 'config.txt';  
	
	public function ConexionMySQL(){ 
	
		if(!isset($this->conexion)){		    
			
			$datos_config = $this->leer_fichero_configuracion($this->nombre_fichero);
			$array_dato_fichero = json_decode($datos_config,true);
									
			$this->conexion = (mysql_connect($array_dato_fichero['host'], $array_dato_fichero['user'], $array_dato_fichero['pass']))or die("Error al crear la conexion".mysql_error());   

			mysql_select_db($array_dato_fichero['base'],$this->conexion) or die("Error al seleccionar la base de datos".mysql_error()); 

		} 

	}   

	public function consultar($consulta){ 

		//$this->total_consultas++;

		$resultado=mysql_query($consulta,$this->conexion);

		if(!$resultado){ 
			echo 'Error al cosultar '.mysql_error(); exit; 
	    } 

		return $resultado; 

	}

	public function fetch_array($consulta){
	   return mysql_fetch_array($consulta);
	}   

	public function num_rows($consulta){

		return mysql_num_rows($consulta);

	}   

	public function getTotalConsultas(){ 
		return $this->total_consultas; 
	}   

	public function close(){
		mysql_close($this->conexion); 
	} 

	private function leer_fichero_configuracion($nombre_fichero){
	       
	       $fichero_texto = fopen ($nombre_fichero, "r");      
	       $contenido_fichero = fread($fichero_texto, filesize($nombre_fichero));
	       return $contenido_fichero;
    }



} 