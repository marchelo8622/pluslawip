-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Servidor: localhost
-- Tiempo de generación: 12-07-2016 a las 19:30:38
-- Versión del servidor: 5.0.51
-- Versión de PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Base de datos: `facturacion`
-- 

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `clientes`
-- 

CREATE TABLE `clientes` (
  `identificacion` varchar(13) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `direccion` varchar(250) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  PRIMARY KEY  (`identificacion`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- Volcar la base de datos para la tabla `clientes`
-- 

INSERT INTO `clientes` VALUES ('1715597090001', 'Marcelo Montalvo', 'P.CARBO  1015 Y COLON', '0995619730');
INSERT INTO `clientes` VALUES ('17155970900', 'Marcelo Montalvo', 'P.CARBO  1015 Y COLON', '0995619730');
INSERT INTO `clientes` VALUES ('1212121212', 'dfdfdsf', 'dfdsfsd', 'nhjmjhgjhg');
INSERT INTO `clientes` VALUES ('1234567890', 'fbgnjghfgfd', 'amazonas y mariana jesus', '2584563');
INSERT INTO `clientes` VALUES ('171559703', 'zoila', 'sur quito', '0995619730');
INSERT INTO `clientes` VALUES ('12125656546', 'prueba1', 'dfdsfsd', 'nhjmjhgjhg');
INSERT INTO `clientes` VALUES ('123445654632', 'prueba3', 'dierfdsfds ', '2584563');
INSERT INTO `clientes` VALUES ('17155922203', 'zoila', 'sur quito', '0995619730');
INSERT INTO `clientes` VALUES ('122227', 'Maria cardenas', '12 octubre', '0995619730');
INSERT INTO `clientes` VALUES ('144446546', 'prueba0', 'dfdsfsd', 'nhjmjhgjhg');
INSERT INTO `clientes` VALUES ('111145654632', 'prueba4', 'dierfdsfds ', '2584563');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `configuraciones`
-- 

CREATE TABLE `configuraciones` (
  `codigo` varchar(10) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `dato1` double(13,2) NOT NULL,
  `dato2` varchar(50) NOT NULL,
  `activo` varchar(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- Volcar la base de datos para la tabla `configuraciones`
-- 

INSERT INTO `configuraciones` VALUES ('PPI', 'parametrizacion porcentaje iva', 14.00, '', 'S');
