<?php

//error_reporting(E_ALL);
//ini_set('display_errors', '1');

require_once('librerias/html2pdf/html2pdf.class.php');

$items_factura = stripslashes($_REQUEST["detalle"]);
$json_items = json_decode($items_factura);

$content = "";
$content .= "
    <style>
         .datosItems {font-size:12px;}
         .datos {font-size:12px;}
      
    </style>
       
        ";


$content .= "    
  <page backtop='48mm' backbottom='5mm' backleft='15mm' backright='40mm' > 
    
    <table class='datos' cellspacing='6'>";


$content .= "   

    <tr>   
        <td style='width:22mm;'>CLIENTE: </td>
        <td style='width:83mm;'>" . $_GET['cliente'] . "</td>
     
        <td>RUC / CI: </td>
        <td>" . $_GET['ruc'] . "</td>
    </tr>
    <tr>   
        <td>DIRECCION: </td>
        <td colspan=3>" . $_GET['direccion'] . "</td>
      
        
    </tr>
    <tr>   
        <td>FECHA: </td>
        <td>" . $_GET['fecha'] . "</td>
   
        <td>TELEFONO: </td>
        <td>" . $_GET['telefono'] . "</td>
    </tr>
    ";


$content .= " </table><br><br /><br /><br />";

$content .= "<table class='datosItems' cellspacing='6' >
  <tr>
    <td style='text-align:left;'><b>CANT.</b></td>
    <td><b>DESCRIPCION</b></td>
    <td><b>VALOR UNITARIO</b></td>
    <td style='text-align:right;'><b>VALOR TOTAL</b></td>
  </tr>";
if(count($json_items) > 0){
foreach ($json_items as $item) {
    if($item->cantidad != "" && $item->descripcion != "" && $item->valorU != "" && $item->valorT != ""  ){
    $content .="
    <tr>
      <td style='width:20mm; text-align:left;'>" . $item->cantidad . "</td>
      <td style='width:70mm; text-align:justify;'>" . $item->descripcion . "</td>
      <td style='width:30mm; text-align:center;'>" . number_format($item->valorU,2,'.',',') . "</td>
      <td style='width:30mm; text-align:right;'>" . number_format($item->valorT,2,'.',',') . "</td>
    </tr>";
    }
}
}
$notas = $_GET['notas'];
if($notas != ""){
$content .="
    <tr><td style='height:5mm;  colspan='5' ></td></tr>
    <tr>
      <td style='width:20mm; text-align:left;'></td>
      <td style='width:70mm; text-align:justify;'><b>*Nota: </b>" . $_GET['notas'] . "</td>
      <td style='width:30mm; text-align:center;'></td>
      <td style='width:30mm; text-align:right;'></td>
    </tr>";
}
$content .="
    <tr><td style='height:50mm; border-bottom:1px dotted; #000;' colspan='5' ></td></tr>
         <tr style='font-weight:bold;'>
              <td style='width:20mm; text-align:center;'></td>
      <td style='width:70mm; text-align:left;''></td>
      <td style='width:30mm; text-align:right;'>SUBTOTAL </td>
      <td style='width:30mm; text-align:right;'>" . number_format($_GET['subtotal'],2,'.',',') . "</td>
             
         </tr>
         
       <tr style='font-weight:bold;'>
              <td style='width:20mm; text-align:center;'></td>
      <td style='width:70mm; text-align:left;''></td>
      <td style='width:30mm; text-align:right;'>I.V.A. </td>
      <td style='width:30mm; text-align:right;'>" . number_format($_GET['iva'],2,'.',',') . "</td>
             
         </tr>
         <tr style='font-weight:bold;'>
              <td style='width:20mm; text-align:center;'></td>
      <td style='width:70mm; text-align:left;''></td>
      <td style='width:30mm; text-align:right;'>TOTAL </td>
      <td style='width:30mm; text-align:right;'>" . number_format($_GET['total'],2,'.',',') . "</td>
             
         </tr>
             ";
$content .= "</table>";

$content .= "</page> ";

/* $html2pdf = new HTML2PDF('P',array('76',$alto),'en',true,'UTF-8', array(0, 0, 0, 0));  
  $html2pdf->WriteHTML($content);
  $html2pdf->Output('facPluslaw.pdf'); */
$html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8');
$html2pdf->WriteHTML($content);
$fecha = date('Y-m-d');
$html2pdf->Output('FAC_pluslawip'.$fecha.'.pdf');






