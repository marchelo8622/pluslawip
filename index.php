<?php 
header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

include("clase_conexion.php");

$obj = new ConexionMySQL();

$consulta = $obj->consultar("SELECT dato1 FROM configuraciones where codigo = 'PPI' and activo = 'S' limit 1"); 
    $iva = 0;
    if($obj->num_rows($consulta)>0){

        while($resultados = $obj->fetch_array($consulta)){ 

                $iva = $resultados['dato1']; 
        } 

    }

$sql = $obj->consultar("SELECT * FROM clientes"); 
   
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Facturacion - PLUSLAWIP</title>
    
    
    
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
   <link href="css/datepicker.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/logo-nav.css" rel="stylesheet">
     <link href="css/jquery.dataTables.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="images/logoplulaw-01.png" alt="">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#">&nbsp;</a>
                    </li>
                    <li>
                        <a href="#">&nbsp;</a>
                    </li>
                    <li>
                        <a href="#">&nbsp;</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Facturaci&oacute;n</h1>


    <div id = "resp"></div>


                 <div align="right"><button href="" class="btn btn-default" onclick = " procesa_cliente();"><span class="glyphicon glyphicon-floppy-disk"></span> Grabar cliente sin facturar</button>    <button href="" class="btn btn-default" data-toggle="modal" data-target="#largeModal"><span class="glyphicon glyphicon-user" ></span> Cargar cliente</button><br /></div>
                 <div style="clear:both; height:8px;"></div>
                <div class="well">
                <table width="100%" cellspacing="7">
                <tr>
                <td>* CLIENTE:</td>
                <td><input type="text" class="form-control" id="cliente" name="cliente" value=""> </td>
                <td style="text-align:right">* RUC / CI:</td>
                <td><input type="text" class="form-control" maxlength="13" id="ruc" name="ruc" onKeyUp="controlaValor('ruc',this);" value=""></td>
                </tr>
                <tr>
                <td colspan="4" height="9"></td>
                </tr>
                <tr>
                <td>* DIRECCION:</td>
                <td colspan="3"><input class="form-control" type="text" id="direccion" name="direccion" value=""></td>
                
                </tr>
                <tr>
                <td colspan="4" height="9"></td>
                </tr>
                <tr>
                <tr>
                <td>* FECHA:</td>
                <td><input type="text" id="fecha_fac" class="form-control" name="fecha" readonly=""></td>
                <td style="text-align:right">* TELEFONO:</td>
                <td><input type="text" id="telefono" class="form-control" name="telefono_fac" onKeyUp="controlaValor('telefono',this);" value=""></td>
                </tr>
                </table>
                <div style="clear:both;"></div>
                </div>
                  <div style="text-align: right; width: 100%;">
        <h2>
           
          <a  onclick="AgregarCampos();" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Nuevo Item</a>
        </h2>
      </div>
                <p>Detalle:</p>
                <table class="table table-striped table-bordered table-condensed" >
                <thead>
                  <tr>
                    <th >Cantidad</th>
                    <th >Descripci&oacute;n</th>
                    <th >Valor Unitario</th>
                    <th >Valor Total</th>
                    <th>Acci&oacute;n</th>                    
                  </tr>
                </thead>
                <tbody id="campos">
                  
                   
                    
                </tbody>
            </table> 
            
            <br />
            <table width="100%">
            <tr>
            <td width="80%"></td>
            <td>SUBTOTAL </td>
            <td><input type="text" id="subtotal" class="form-control" readonly></td>
            </tr>
            <tr>
            <td></td>
            <td><input type="checkbox" id="valida_iva" checked=""> IVA </td>
            <td><input type="text" id="iva" class="form-control" readonly></td>
            </tr>
            <tr>
            <td></td>
            <td>TOTAL </td>
            <td><input type="text" id="total" class="form-control" readonly></td>
            </tr>
            </table>
            <br />
            <p class="alert alert-warning">Aviso!... Si desea escribir alguna observaci&oacute;n en la factura haga click en el siguiente icono <a style="cursor:pointer;" title="abrir / cerrar notas"><img src="images/notes.png" id="btn_notas"></a>.</p>
            <div id="divocultarnotas" style="display:none;">
            <table width="100%">
            <tr>
            
            <td valign="top" width="10%"><b>Notas:</b> </td>
            <td><textarea class="form-control" id="notas"></textarea></td>
            </tr>
            </table> 
            <br />
            </div>
                <br />       
                <div class="well">
                 <div style="clear:both;"></div>
           <div class="form-actions">
             
            <button onClick="generar_pdf();" id="btn_guarda_orden_produccion" class="btn btn-primary" type="button"><span class="glyphicon glyphicon-list-alt"></span> Generar Factura PDF & Grabar Cliente</button>
           
           <button href="" class="btn btn-success" data-toggle="modal"  data-target="#smallModal"><span class="glyphicon glyphicon-usd"></span> Parametrizaci&oacute;n IVA  </button>
            <button href="" class="btn btn-default" data-toggle="modal" data-target="#largeModal"><span class="glyphicon glyphicon-user" ></span>  Listado Clientes  </button>
           
           
            <button href="" class="btn btn-warning" onClick="recargar()"><span class="glyphicon glyphicon-refresh"></span>  Limpiar Pantalla  </button>
          </div>

          <input type="hidden" value="<?php echo $iva; ?>" id="porcentaje_iva">

          <div class="modal fade" id="smallModal" tabindex="-1" role="dialog" aria-labelledby="smallModal" aria-hidden="true">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Parametrizaci&oacute;n IVA</h4>
              </div>
              <div class="modal-body">
                <p>Porcentaje de iva:</p>
                <input type="text" id = "customIva" class="form-control" onkeyup="controlaValorNumerico('customIva',this)" value="<?php echo $iva; ?>">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onClick="guarda_iva()">Guardar cambios</button>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" id="close" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Datos clientes</h4>
          </div>
          <div class="modal-body"> 

                        <p align="center"><a title="exportar" style="cursor:pointer"  onclick="exportar()"><img src="images/excel.png" /></a></p>
            
                        <table id="clientesTable" class="display" style="font-size: 12px;" width="100%">

                            <thead>
                            <tr>
                                <th>Ci / Ruc </th>
                                <th>Nombre </th>
                                <th>Direcci&oacute;n </th>
                                <th>Tel&eacute;fono</th>
                                <th>Acciones</th>
                            </tr>
                                  
                            </thead>
                            <tbody>
                <?php if($obj->num_rows($sql)>0){ ?>

                        <?php while($resultados = $obj->fetch_array($sql)){ ?>

                                     <tr >
                                          <td><?php echo $resultados['identificacion']; ?> </td>
                                          <td><?php echo $resultados['nombre']; ?></td>
                                          <td><?php echo $resultados['direccion']; ?></td>
                                          <td><?php echo $resultados['telefono']; ?></td>
                                          <td align="right">

                                            <button type="button" onclick="cargar_cliente('<?php echo $resultados['identificacion']; ?>','<?php echo $resultados['nombre']; ?>','<?php echo $resultados['direccion']; ?>','<?php echo $resultados['telefono']; ?>');" class="btn btn-default btn-sm">
                                              <span class="glyphicon glyphicon-ok"></span> Cargar
                                            </button>


                                            <button type="button" onclick="elimina_cliente('<?php echo $resultados['identificacion']; ?>');" class="btn btn-default btn-sm">
                                              <span class="glyphicon glyphicon-trash"></span> Eliminar 
                                            </button></td>

                                     </tr>

                        <?php } ?>

                        <?php } ?>
                    </tbody>

                       </table>

                       <br style="clear:both;">

        
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
          </div>
        </div>
      </div>
    </div>
                
                
                </div>
            </div>
        </div>
    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->

<script src="js/jquery.dataTables1.10.min.js"></script>

<script src="js/FileSaver.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/custom.js"></script>
    

</body>

</html>
