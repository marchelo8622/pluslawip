// JavaScript Document
$(document).ready(function () {

    $('#clientesTable').DataTable({
        "language": {
            "lengthMenu": "Ver _MENU_ entradas",
            "zeroRecords": "Lo sentimos - no existen resultados de búsqueda",
            "info": "Mostrando _PAGE_ de _PAGES_ páginas",
            "infoEmpty": "No existen resultados",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar",
            "previous": "Anterior",
            "next": "Siguiente"
        },
        "iDisplayLength": 5,
        "aLengthMenu": [[5, 10], [5, 10]]
        //"scrollY":        "300px",
        //"scrollCollapse": false,
        //"paging":         false,
        //"searching":true
        //"info":false
    });
    
    $('#fecha_fac').datepicker({        
        today: 'Hoy',
       // todayBtn: true,
        clear: "Borrar",
        weekStart: 1,       
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true
    });
	
	 $("#valida_iva").click(function() {  
         
            calcula_totales_factura();  
         
    });
	 
	$("#btn_notas").click(function () {
      $("#divocultarnotas").each(function() {
        displaying = $(this).css("display");
        if(displaying == "block") {
          $(this).fadeOut('slow',function() {
           $(this).css("display","none");
          });
        } else {
          $(this).fadeIn('slow',function() {
            $(this).css("display","block");
          });
        }
      });
    }); 


});



var ventana_principal;

function recargar() {
    location.reload(true);
}

function controlaValor(campo, valor) {
    var inputVal = valor.value;
    var numericReg = /\.(?=\d*[.]\d*)/g;
    if (!numericReg.test(inputVal)) {
        inputVal = inputVal.replace(/[^0-9.]/g, "");
        document.getElementById(campo).value = inputVal;
    }
}

function calcula_valor_total(id_val_unitario, id_cantidad, id_valor_total) {
    //alert('entro');
    var valor_unitario = $('#' + id_val_unitario).val();
    var cantidad = $('#' + id_cantidad).val();
    var result = cantidad * valor_unitario;

    $('#' + id_valor_total).val(result.toFixed(2));
    calcula_totales_factura();
}

function imprimir(cliente, ruc, direccion, telefono, fecha, iva, total, subtotal,notas,items, parm) {

    var ancho = 750;
    var alto = 820;
    var posicion_x;
    var posicion_y;
    posicion_x = (screen.width / 2) - (ancho / 2);
    posicion_y = (screen.height / 2) - (alto / 2);
    ventana_imprimir = window.open("imprimir.php?cliente=" + cliente + "&ruc=" + ruc + "&direccion=" + direccion + "&telefono=" + telefono + "&fecha=" + fecha + "&total=" + total + "&iva=" + iva + "&subtotal=" + subtotal + "&notas="+notas+"&detalle=" + items + "&P=" + parm + "", "ventana", "width=" + ancho + ",height=" + alto + ",left=" + posicion_x + ",top=" + posicion_y + "");
    //AbriVentanaModalConScroll("imprimir.php?fac=" + numFac + "&ID="+id+"&IDB="+idb+"&Productos="+Productos+"", "popup", "width=" + ancho + ",height=" + alto + ",left=" + posicion_x + ",top=" + posicion_y + "");
}

function calcula_totales_factura() {

    var sumValor;
    var valor_iva;
    var valor_tot_fac;
    sumValor = 0;
    $(".sumValor").each(function () {
        if ($(this).val() != "") {
            sumValor = sumValor + eval($(this).val());
        }
    });
     if($("#valida_iva").is(':checked')) {
            var porcentaje_iva = $('#porcentaje_iva').val();
            
            valor_iva = (sumValor * porcentaje_iva) / 100; 
     } else {  
            valor_iva = 0;  
     }  
    
    valor_tot_fac = sumValor + valor_iva;

    $('#subtotal').val(sumValor.toFixed(2));
    $('#iva').val(valor_iva.toFixed(2));
    $('#total').val(valor_tot_fac.toFixed(2));


}

var arregloProducto = new Array();
var nextinput = 0;
function AgregarCampos() {
    nextinput++;

    campo = '<tr id="rut' + nextinput + '"><td><input class="form-control" type="text" size="15" onkeyup="controlaValor(\'cantidad' + nextinput + '\',this);" id="cantidad' + nextinput + '"  name="cantidad' + nextinput + '" onchange="calcula_valor_total(\'valorU' + nextinput + '\',\'cantidad' + nextinput + '\',\'valorT' + nextinput + '\');"    /></td><td><input class="form-control" type="text" size="85" id="descripcion' + nextinput + '"  name="descripcion' + nextinput + '" /></td><td><input class="form-control" type="text" size="15" onkeyup="controlaValor(\'valorU' + nextinput + '\',this);" id="valorU' + nextinput + '"  name="valorU' + nextinput + '" onchange="calcula_valor_total(\'valorU' + nextinput + '\',\'cantidad' + nextinput + '\',\'valorT' + nextinput + '\');"   /></td><td ><input class="sumValor form-control" type="text" size="10"  id="valorT' + nextinput + '"  name="valorT' + nextinput + '"  readonly/></td><td id="eliminar" ><a class="btn btn-danger" ><span class="glyphicon glyphicon-remove"></span> Quitar</a></td></tr>';
    $("#campos").append(campo);
    arregloProducto[arregloProducto.length] = ({cantidad: '', descripcion: '', valorU: '', valorT: ''});


}

function generar_pdf() {

    var cliente = $('#cliente').val();
    var ruc = $('#ruc').val();
    var direccion = $('#direccion').val();
    var telefono = $('#telefono').val();
    var fecha = $('#fecha_fac').val();
    var iva = $('#iva').val();
    var total = $('#total').val();
    var subtotal = $('#subtotal').val();
	var notas = $('#notas').val();

    if (cliente == "" || ruc == "" || direccion == "" || fecha == "" || telefono == "" ) {

        alert("Los datos marcados con * son obligatorios.");
    } else if(total == "" || total == 0 ){
        
        alert("No existen detalles en la factura.");
        
    }else {
        parm = new Date().getTime();
        for (var i = 0; i < arregloProducto.length; i++) {
            arregloProducto[Number(i)].cantidad = $('#cantidad' + (i + 1)).val();
            arregloProducto[Number(i)].descripcion = $('#descripcion' + (i + 1)).val();
            arregloProducto[Number(i)].valorU = $('#valorU' + (i + 1)).val();
            arregloProducto[Number(i)].valorT = $('#valorT' + (i + 1)).val();
        }
        var jsonItems = JSON.stringify(arregloProducto);

        if (confirm('¿Estás seguro de realizar esta acción?')) {
            procesa_cliente();
            imprimir(cliente, ruc, direccion, telefono, fecha, iva, total, subtotal,notas,jsonItems, parm);
            return false;
        }
    }
}


function guarda_iva() {
    var customIva = $('#customIva').val();

    if(customIva == ""){
        alert('Ingrese un valor de porcentaje de iva');
    }else{
        $.ajax({
            url: "ajax.php",
            type: 'POST',
            data: {iva: customIva, flag:'iva'},
            success: function (data) {
                
                    alert(data);

                    recargar();
            
            }
        });
    }
}

function procesa_cliente() {
    var identificacion = $('#ruc').val();
    var nombre = $('#cliente').val();
    var direccion = $('#direccion').val();
    var telefono = $('#telefono').val();

    if(identificacion == '' || nombre =='' || direccion == '' || telefono == ''){

        alert('Los campos con (*) son obligatorios');

    }else{  

    
        $.ajax({
            url: "ajax.php",
            type: 'POST',
            data: {identificacion: identificacion,nombre: nombre,direccion: direccion,telefono:telefono, flag:'custom_cliente'},
            success: function (data) {

             var msj =  '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> '+data+' </div>'; 

            $('#resp').html(msj);

            setTimeout(function () {
                $("#resp").fadeOut(800).fadeIn(800).fadeOut(800).fadeIn(800).fadeOut(300);

                recargar();
                                    
            }, 1100);

            
         
                                    
            
            }
        });
    }
    
}




function elimina_cliente(identificacion) {
        if (confirm('¿Estás seguro de realizar esta acción?')) {     
            $.ajax({
                url: "ajax.php",
                type: 'POST',
                data: {identificacion: identificacion, flag:'delete_cliente'},
                success: function (data) {            
                      alert(data);
                      recargar();                  
                
                }
            });
        }
    
}

function exportar() {
        
        window.location = 'ajax.php?flag=exportar';
    
}


function cargar_cliente(identificacion,nombre,direccion,telefono) {
    $('#ruc').val(identificacion);
    $('#cliente').val(nombre);
    $('#direccion').val(direccion);
    $('#telefono').val(telefono); 

    $( "#close" ).click();
}

function recargar() {
    location.reload(true);
}

function controlaValorNumerico(campo, valor) {
    var inputVal = valor.value;
    var numericReg = /\.(?=\d*[.]\d*)/g;
    if (!numericReg.test(inputVal)) {
        inputVal = inputVal.replace(/[^0-9.]/g, "");
        document.getElementById(campo).value = inputVal;
    }
}

$(document).on("click", "#eliminar", function () {
    var parent = $(this).parent();
    $(parent).remove();
    calcula_totales_factura();
});






